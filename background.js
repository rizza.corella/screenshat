const screenshots = []

chrome.action.onClicked.addListener(function (tab) {
    console.log('Adding choose desktop media listener...')
    chrome.desktopCapture.chooseDesktopMedia([
        "tab"
    ], tab, (streamId) => {
        console.log(`Received stream ID: ${streamId}`)
        //check whether the user canceled the request or not
        if (streamId && streamId.length) {
            setTimeout(() => {
                chrome.tabs.sendMessage(tab.id, {name: "stream", streamId}, (response) => {
                    console.log('Received response')
                    if (response.success) {
                        screenshots.push(response.message)
                        console.log('Response is successful. Received:')
                        console.log(response.message)
                        console.log(`Have now collected a total of ${screenshots.length} screenshots`)
                    }
                })
            }, 200)
        }
    })
})